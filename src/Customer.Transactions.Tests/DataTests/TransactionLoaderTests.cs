﻿using System.Collections.Generic;
using System.IO;
using Customer.Transactions.Common.Config;
using Customer.Transactions.Data.Loader;
using Moq;
using NUnit.Framework;

namespace Customer.Transactions.Tests.DataTests
{
    [TestFixture]
    public class TransactionLoaderTests
    {
        [Test]
        public void LoadTransactionsShouldSucceed()
        {
            Mock<IConfigManager> manager = new Mock<IConfigManager>();
            manager.Setup(s => s.Read("Customers.Data")).Returns($@"{TestContext.CurrentContext.TestDirectory}\DataTests\Data\Customers.csv");
            manager.Setup(s => s.Read("Transactions.Data")).Returns($@"{TestContext.CurrentContext.TestDirectory}\DataTests\Data\Transactions.csv");

            ITransactionLoader loader = new TransactionLoader(manager.Object);
            List<Common.Model.Customer> customers = loader.LoadData();

            Assert.AreEqual(3, customers.Count);

            Assert.AreEqual(2, customers[0].Accounts.Count);
            Assert.AreEqual(5, customers[0].Accounts[0].Transactions.Count);
            Assert.AreEqual(2, customers[0].Accounts[1].Transactions.Count);

            Assert.AreEqual(1, customers[1].Accounts.Count);
            Assert.AreEqual(4, customers[1].Accounts[0].Transactions.Count);

            Assert.AreEqual(0, customers[2].Accounts.Count);

            Assert.AreEqual(3000m, customers[0].Balance);
            Assert.AreEqual(2555m, customers[1].Balance);
            Assert.AreEqual(0m, customers[2].Balance);

            Assert.AreEqual(1800m, customers[0].Accounts[0].Balance);
            Assert.AreEqual(1200m, customers[0].Accounts[1].Balance);
            Assert.AreEqual(2555m, customers[1].Accounts[0].Balance);
        }

        [Test]
        public void LoadTransactionsShouldFailFileNotFound()
        {
            Mock<IConfigManager> manager = new Mock<IConfigManager>();
            manager.Setup(s => s.Read("Customers.Data")).Returns($@"{TestContext.CurrentContext.TestDirectory}\DataTests\Data\Customers_NOT_FOUND.csv");
            manager.Setup(s => s.Read("Transactions.Data")).Returns($@"{TestContext.CurrentContext.TestDirectory}\DataTests\Data\Transactions.csv");

            ITransactionLoader loader = new TransactionLoader(manager.Object);
            Assert.Throws<FileNotFoundException>(() => loader.LoadData());
        }
    }
}
