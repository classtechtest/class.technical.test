﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Customer.Transactions.Data.Loader;
using Customer.Transactions.Data.Repository;

namespace Customer.Transactions.Data.Ioc
{
    public class TransactionDataModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            // Register as singleton
            builder.RegisterType<TransactionLoader>().As<ITransactionLoader>().SingleInstance();
            builder.RegisterType<CustomerTransactionRepository>().As<IRepository<Common.Model.Customer>>();
        }
    }
}
