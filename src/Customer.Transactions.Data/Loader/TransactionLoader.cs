﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Customer.Transactions.Common.Config;
using Customer.Transactions.Common.Model;

namespace Customer.Transactions.Data.Loader
{
    public class TransactionLoader : ITransactionLoader
    {
        private const string CustomersDataSetting = "Customers.Data";
        private const string TransactionsDataSetting = "Transactions.Data";

        private readonly IConfigManager _configManager;
        private Dictionary<int, Common.Model.Customer> _customers;
		private readonly object _lockObject = new object();

        public TransactionLoader(IConfigManager configManager)
        {
            _configManager = configManager;
        }

        /// <summary>
        /// Loads data from data source and builds object model
        /// </summary>
        public List<Common.Model.Customer> LoadData()
        {
            // Only read data once
            if (_customers == null)
            {
				lock(_lockObject)
				{
					// Use double-checked locking
					if(_customers == null)
					{
						_customers = new Dictionary<int, Common.Model.Customer>();
						
						string customersPath = _configManager.Read(CustomersDataSetting);
						string transactionsPath = _configManager.Read(TransactionsDataSetting);

						if (!File.Exists(customersPath) || !File.Exists(transactionsPath))
						{
							throw new FileNotFoundException($"Check files exist at {customersPath} and {transactionsPath}");
						}

						string[] customerData = File.ReadAllLines(customersPath);
						string[] transactionData = File.ReadAllLines(transactionsPath);

						ReadCustomers(customerData);               
						ReadTransactions(transactionData);
					}
				}
            }

            return _customers.Values.ToList();
        }

        private void ReadCustomers(string[] data)
        {
            foreach (string line in data)
            {
                string[] custData = line.Split(',');

                Common.Model.Customer customer = new Common.Model.Customer();
                customer.Id = Convert.ToInt32(custData[0]);
                customer.FirstName = custData[1];
                customer.LastName = custData[2];
                customer.Accounts = new List<Account>();

                _customers.Add(customer.Id, customer);
            }
        }

        private void ReadTransactions(string[] data)
        {
            Dictionary<int, Account> accounts = new Dictionary<int, Account>();

            foreach (string line in data)
            {
                string[] transactionData = line.Split(',');

                Transaction transaction = CreateTransaction(transactionData);

                Account account;

                if (accounts.ContainsKey(transaction.AccountId))
                {
                    account = accounts[transaction.AccountId];
                }
                else
                {
                    account = CreateAccount(transactionData);
                    accounts.Add(transaction.AccountId, account);
                }

                account.Transactions.Add(transaction);
            }

            foreach (Account acc in accounts.Values)
            {
                if (_customers.ContainsKey(acc.CustomerId))
                {
                    _customers[acc.CustomerId].Accounts.Add(acc);
                }
                else
                    throw new Exception($"Customer with Id {acc.CustomerId} not found");
            }
        }

        private Account CreateAccount(string[] accountData)
        {
            Account account = new Account();
            account.Id = Convert.ToInt32(accountData[1]);
            account.CustomerId = Convert.ToInt32(accountData[0]);
            account.Name = $"Account {accountData[1]}";
            account.Transactions = new List<Transaction>();

            return account;
        }

        private Transaction CreateTransaction(string[] transactionData)
        {
            Transaction transaction = new Transaction();
            transaction.CustomerId = Convert.ToInt32(transactionData[0]);
            transaction.AccountId = Convert.ToInt32(transactionData[1]);
            transaction.Amount = Convert.ToDecimal(transactionData[2]);
            transaction.Code = transactionData[3];
            transaction.Balance = Convert.ToDecimal(transactionData[4]);
            transaction.Date = DateTime.Parse(transactionData[5], CultureInfo.InvariantCulture);

            return transaction;
        }
    }
}
