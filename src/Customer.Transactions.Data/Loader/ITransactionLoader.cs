﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Transactions.Data.Loader
{
    public interface ITransactionLoader
    {
        List<Common.Model.Customer> LoadData();
    }
}
