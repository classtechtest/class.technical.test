﻿using System;
using System.Collections.Generic;
using System.Text;
using Customer.Transactions.Data.Loader;

namespace Customer.Transactions.Data.Repository
{
    public class CustomerTransactionRepository : IRepository<Common.Model.Customer>
    {
        private readonly ITransactionLoader _loader;

        public CustomerTransactionRepository(ITransactionLoader loader)
        {
            _loader = loader;
        }

        public List<Common.Model.Customer> GetAll()
        {
            return _loader.LoadData();
        }
    }
}
