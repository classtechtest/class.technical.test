﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Customer.Transactions.Data.Repository
{
    public interface IRepository<T>
    {
        List<T> GetAll();
    }
}
