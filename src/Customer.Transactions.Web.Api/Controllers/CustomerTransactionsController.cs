﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using Customer.Transactions.Service.Service;

namespace Customer.Transactions.Web.Api.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api")]
    public class CustomerTransactionsController : ApiController
    {
        private readonly ICustomerTransactionService _service;

        public CustomerTransactionsController(ICustomerTransactionService service)
        {
            _service = service;
        }

        [HttpGet]
        [Route("customers")]
        public List<Common.Model.Customer> GetCustomerTransactions()
        {
            return _service.GetCustomerTransactions();
        }
    }
}
