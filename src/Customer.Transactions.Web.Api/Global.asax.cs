﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using Autofac;
using Autofac.Integration.WebApi;
using Customer.Transactions.Common.Ioc;
using Customer.Transactions.Data.Ioc;
using Customer.Transactions.Service.Ioc;
using Customer.Transactions.Web.Api.Controllers;

namespace Customer.Transactions.Web.Api
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            log4net.Config.XmlConfigurator.Configure();

            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterModule<CommonModule>();
            builder.RegisterModule<TransactionDataModule>();
            builder.RegisterModule<TransactionServiceModule>();

            builder.RegisterApiControllers(typeof(CustomerTransactionsController).Assembly);

            HttpConfiguration config = new HttpConfiguration();
            builder.RegisterWebApiFilterProvider(config);

            IContainer container = builder.Build();
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }
    }
}
