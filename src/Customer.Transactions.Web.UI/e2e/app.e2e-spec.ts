import { Customer.Transactions.Web.UIPage } from './app.po';

describe('customer.transactions.web.ui App', () => {
  let page: Customer.Transactions.Web.UIPage;

  beforeEach(() => {
    page = new Customer.Transactions.Web.UIPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
