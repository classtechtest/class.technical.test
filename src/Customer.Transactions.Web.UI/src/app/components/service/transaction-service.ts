import { Injectable } from '@angular/core';
import {Http, Headers, RequestOptions, Response} from '@angular/http';
import {environment} from '../../../environments/environment';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class TransactionService {
  url: string;
  error: string;
  constructor(private http: Http) {
    this.url = environment.transactionapihost;
  }

  getTransactions(): Observable<Response> {
    const headers = new Headers({'Content-Type': 'application/json'});
    const options = new RequestOptions({headers: headers});

    return this.http.get(this.url, options);
  }
}
