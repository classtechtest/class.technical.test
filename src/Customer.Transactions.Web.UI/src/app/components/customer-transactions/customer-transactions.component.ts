import {Component, OnInit} from '@angular/core';
import {Customer} from '../model/customer';
import {TransactionService} from '../service/transaction-service';

@Component({
  selector: 'app-customer-transactions',
  templateUrl: './customer-transactions.component.html',
  styleUrls: ['./customer-transactions.component.css']
})

export class CustomerTransactionsComponent implements OnInit {
  customers: Customer[];
  loading = true;

  constructor(private transactionservice: TransactionService) {
  }

  ngOnInit(): void {
    this.transactionservice.getTransactions().subscribe(t => {
      this.customers = t.json();
      this.loading = false;
    });
  }

  getCustomerRowColour(rowData, rowIndex): string {
    if (rowData.Accounts.length > 1) {
      return 'multiple-account-row';
    }
    if (rowData.Accounts.length === 0) {
      return 'no-account-row';
    }

    return '';
  }

  getTransactionRowColour(rowData, rowIndex): string {
    const tranDate = new Date(Date.parse(rowData.Date));

    if (tranDate.getHours() === 18 && tranDate.getMinutes() > 0) {
      return 'transaction-overnight';
    }

    if (tranDate.getHours() > 18 || tranDate.getHours() < 6) {
      return 'transaction-overnight';
    }

    if (rowData.Code === 'COT' && rowData.Amount > rowData.Balance) {
      return 'transaction-over-half-balance';
    }

    return '';
  }
}
