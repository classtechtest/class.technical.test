import {CustomerAccount} from './account';

export interface Customer {
  Id: number;
  FirstName: string;
  LastName: string;
  Accounts: CustomerAccount[];
}
