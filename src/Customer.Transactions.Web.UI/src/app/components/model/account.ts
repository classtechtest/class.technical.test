import {Transaction} from './transaction';

export interface CustomerAccount {
  Id: number;
  Name: string;
  CustomreId: number;
  Transactions: Transaction[];
}
