export interface Transaction {
  CustomerId: number;
  AccountId: number;
  Amount: number;
  Code: string;
  Balance: number;
  Date: Date;
}
