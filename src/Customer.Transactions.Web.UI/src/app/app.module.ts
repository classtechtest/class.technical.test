import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {DataTableModule} from 'primeng/primeng';

import { AppComponent } from './app.component';
import {CustomerTransactionsComponent} from './components/customer-transactions/customer-transactions.component';
import {TransactionService} from './components/service/transaction-service';

@NgModule({
  declarations: [
    AppComponent,
    CustomerTransactionsComponent
  ],
  imports: [
    BrowserModule,
    DataTableModule,
    HttpModule
  ],
  providers: [TransactionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
