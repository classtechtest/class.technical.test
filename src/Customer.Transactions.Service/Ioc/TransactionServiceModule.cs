﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Autofac;
using Customer.Transactions.Service.Service;

namespace Customer.Transactions.Service.Ioc
{
    public class TransactionServiceModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CustomerTransactionService>().As<ICustomerTransactionService>();
        }
    }
}
