﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Customer.Transactions.Service.Service
{
    public interface ICustomerTransactionService
    {
        List<Common.Model.Customer> GetCustomerTransactions();
    }
}
