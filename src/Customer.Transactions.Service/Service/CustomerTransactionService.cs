﻿using System;
using System.Collections.Generic;
using System.Text;
using Customer.Transactions.Data.Repository;

namespace Customer.Transactions.Service.Service
{
    public class CustomerTransactionService : ICustomerTransactionService
    {
        private readonly IRepository<Common.Model.Customer> _repository;

        public CustomerTransactionService(IRepository<Common.Model.Customer> repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets all customer transactions
        /// </summary>
        public List<Common.Model.Customer> GetCustomerTransactions()
        {
            return _repository.GetAll();
        }
    }
}
