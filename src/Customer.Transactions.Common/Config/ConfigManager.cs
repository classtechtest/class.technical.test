﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Transactions.Common.Config
{
    public class ConfigManager : IConfigManager
    {
        /// <summary>
        /// Reads the config setting
        /// </summary>
        public string Read(string setting)
        {
            return ConfigurationManager.AppSettings.AllKeys.Contains(setting) ? ConfigurationManager.AppSettings[setting] : throw new Exception($"Setting {setting} not found");
        }
    }
}
