﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Customer.Transactions.Common.Config
{
    public interface IConfigManager
    {
        string Read(string setting);
    }
}
