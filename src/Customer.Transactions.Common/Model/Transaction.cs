﻿using System;
using System.Runtime.Serialization;

namespace Customer.Transactions.Common.Model
{
    [DataContract]
    public class Transaction
    {
        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public int AccountId { get; set; }

        [DataMember]
        public decimal Amount { get; set; }

        [DataMember]
        public string Code { get; set; }

        [DataMember]
        public decimal Balance { get; set; }

        [DataMember]
        public DateTime Date { get; set; }
    }
}
