﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Customer.Transactions.Common.Model
{
    [DataContract]
    public class Account
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public int CustomerId { get; set; }

        [DataMember]
        public decimal Balance
        {
            get
            {
                Transaction last = Transactions?.OrderByDescending(t => t.Date).FirstOrDefault();

                return last?.Balance ?? 0.00m;
            }
            set { }
        }

        [DataMember]
        public List<Transaction> Transactions { get; set; }
    }
}
