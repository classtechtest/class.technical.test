﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Customer.Transactions.Common.Model
{
    [DataContract]
    public class Customer
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string FirstName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public decimal Balance
        {
            get
            {
                decimal? balance = Accounts?.Sum(t => t.Balance);

                return balance.GetValueOrDefault();
            }
            set {}
        }

        [DataMember]
        public List<Account> Accounts { get; set; }
    }
}
