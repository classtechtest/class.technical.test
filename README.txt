The application has been written using .NET WebApi and Angular

The application has been deployed to AWS and can be accessed here http://52.63.100.99/

The API layer can be viewed using swagger at http://52.63.100.99/customer-transactions/swagger/


To Run the application locally

1) Open the Customer.Transactions.sln solution file in Visual Studio and update the Web.config in the Customer.Transactions.Web.Api project. Set the Customers.Data and Transactions.Data app settings to point to the location of the data files in the solution in the "Sample_Data" folder. If you want to use your own version of the files the header rows must be removed.

2) Run the Customer.Transactions.Web.Api project.

3) Navigate to http://localhost:8606/swagger to ensure the API is running correctly

4) Ensure Node.js is insalled.

5) Navigate to the Customer.Transactions.Web.UI and run "npm install" from the command line

6) Run "ng serve" from the command line and navigate to http://localhost:4200 and the solution should be displyed
